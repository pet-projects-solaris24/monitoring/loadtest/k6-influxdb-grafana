data "aws_ami" "ubuntu_jammy" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_security_group" "test" {
  name = "test"
  description = "Enable SSH access on Port 22"
  vpc_id = aws_vpc.main.id

  ingress {
    description = "SSH Access"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8086
    to_port     = 8086
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "SSH Security Group"
  }

}


# Визначаємо ключ для SSH доступу
resource "aws_key_pair" "example_key" {
  key_name   = "k6-demo"
  public_key = file("~/.ssh/demo-cluster.pub") # змініть шлях до свого файлу ключа
}

resource "aws_instance" "test" {
  ami           = data.aws_ami.ubuntu_jammy.id
  instance_type = "t3a.small"
  subnet_id     = aws_subnet.public_us_east_1a.id
  key_name      = aws_key_pair.example_key.key_name 
  associate_public_ip_address = true

  vpc_security_group_ids = [
    aws_security_group.test.id
  ]

 tags = {
    Name  = "k6"
 }
  lifecycle {
    create_before_destroy = true
  }
   depends_on = [ aws_vpc.main, aws_security_group.test ]
}


data "template_file" "playbook" {
  template = file("../ansible/playbook.yml")
}

resource "null_resource" "script" {
  triggers = {
    file = filesha1("../ansible/playbook.yml")
  }

  connection {
    type        = "ssh"
    host        = aws_instance.test.public_ip
    user        = "ubuntu"
    private_key = file("~/.ssh/demo-cluster")
  }

  provisioner "file" {
    source      = "../ansible"
    destination = "/home/ubuntu"
  }

  provisioner "file" {
    source      = "../k6"
    destination = "/home/ubuntu"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo apt install -y ansible",
      "ansible-playbook /home/ubuntu/ansible/playbook.yml"
    ]
  }

   depends_on = [ aws_instance.test, aws_security_group.test ]
}

# Виводимо параметри підключення до створеного EC2 інстансу
output "instance_connection" {
  value = "ssh ${aws_instance.test.public_ip} -l ubuntu -i ~/.ssh/demo-cluster"
}