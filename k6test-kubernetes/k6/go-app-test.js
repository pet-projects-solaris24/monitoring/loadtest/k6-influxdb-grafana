
import http from "k6/http";
import { check, sleep } from "k6";

export let options = {
  vus: 10,
  duration: "880s"
};

export default function() {
  let res = http.get("http://go-app.staging:8001/api/devices");
  check(res, { "status is 200": (r) => r.status === 200 });
  sleep(1);

  let payload = JSON.stringify({ mac: "12-34-56-78-90-AB", firmware: "2.1.6" });
  res = http.post("http://go-app.staging:8001/api/devices", payload, {
    headers: { "Content-Type": "application/json" }
  });
  check(res, { "status is 201": (r) => r.status === 201 });
  sleep(1);
}
