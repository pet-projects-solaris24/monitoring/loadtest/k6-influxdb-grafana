# Using
https://k6.io/blog/running-distributed-tests-on-k8s/
https://k6.io/docs/get-started/running-k6/
https://medium.com/@nairgirish100/k6-with-docker-compose-influxdb-grafana-344ded339540

Install K8s CRD
------------------
cd k6-operator
make deploy

Set test.js like ConfigMap
-----------------------------
kubectl get all -n k6-operator-system
kubectl create configmap crocodile-stress-test -n k6-operator-system --from-file ../k6/test.js


Start Test
----------

kubectl create configmap go-app-test -n staging --from-file ./go-app-test.js
kubectl apply -f ./go-app-custom-resource.yml

kubectl apply -f ../k6/custom-resource.yml  # where --out influxdb=http://3.87.77.250:8086 it's public IP EC2 with influxdb
kubectl get k6 -n k6-operator-system
kubectl get pods -n k6-operator-system

If change custom-resourse.yml paramrters you need recreate ConfigMap
kubectl delete -f ./custom-resource.yml
kubectl apply -f ../k6/custom-resource.yml

Copy dashboard k6-load-testing-results_rev3.json
c:\Devops\AntonSavchuk_Pets-projects\Monitoring\k6-influxdb-Grafana\k6\grafana\dashboards\


Как делать тесты test.js
--------------------------
- из HAR (записывкет действия юзера в браузере в виде архива har и далее этот архив конвертиться в js командой k6)
- попросить ChatGTP написать js исходя из кода сайта
